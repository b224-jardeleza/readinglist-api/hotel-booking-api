const mongoose = require("mongoose");

const roomSchema = new mongoose.Schema({
	roomNo: {
		type: Number,
		required: [true, 'Room Number is required!']
	},
	roomType: {
		type: String,
		required: [true, 'Room Type is required!']
	},
	bookingPrice: {
		type: Number,
		required: [true, 'Total Amount is required!'],
		default: 0
	},
	isActive: {
		type: Boolean,
		default: true
	}
});

module.exports = mongoose.model("Room", roomSchema);