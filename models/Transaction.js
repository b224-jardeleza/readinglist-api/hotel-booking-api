const mongoose = require("mongoose");

const transactionSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required!"]
	},
	rooms: [
		{	
			roomNo: {
				type: String,
				required: [true, "Room ID is required!"]
			},
			days: {
				type: Number,
				min: 1,
				default: 1
			},
			subtotal: {
				type: Number,
				required: [true, "Subtotal is required"],
				default: 0 
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required!"],
		default: 0
	},
	checkedIn: {
		type: Date,
		default: new Date
	}
});

module.exports = mongoose.model("Transaction", transactionSchema);