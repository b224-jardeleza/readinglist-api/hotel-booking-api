// Server Setup

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const roomRoutes = require("./routes/roomRoutes");
const transactionRoutes = require("./routes/transactionRoutes");


const app = express();
const port = process.env.PORT || 8080;

mongoose.set("strictQuery", false);

// DB Setup
mongoose.connect('mongodb+srv://jardeleza-224:admin123@batch224-jardeleza.gaqgipf.mongodb.net/hotel-booking-API?retryWrites=true&w=majority',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", () => console.log("Connection Error"));
db.once("open", () => console.log("Connected to MongoDB"));


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Main URI
app.use("/users", userRoutes);
app.use("/rooms", roomRoutes);
app.use("/transactions", transactionRoutes);


app.listen(port, () => {console.log(`API is now running at localhost port: ${port}`)});