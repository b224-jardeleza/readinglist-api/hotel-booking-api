const User = require("../models/User");
const Product = require("../models/Room");
const Order = require("../models/Transaction");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Register User
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 12)
	})

	// Check for any duplicate
	return User.findOne({email: reqBody.email}).then(result => {

		if(result){
			
			return "User already exists"
		
		} else {

			// continue to register user if do not exist
			return newUser.save().then((user, error) => {

				if(error){
					return false
				} else {
					return "Registration successful!"
				}
			})

		}
	})	
};


// Login User
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result, error) => {

		if(error){

			return false

		} else {

			if(result == null){

				return "Email is not registered!"

			} else {

				const isPassWordCorrect = bcrypt.compareSync(reqBody.password, result.password)

				if(isPassWordCorrect) {

					return {access: auth.createAccessToken(result)}

				} else {

					return "Password is not correct"
				}
			}
		}
		
	})
};


// Get User Profile
module.exports.getProfile = (reqBody) => {
	
	return User.findById(reqBody.id).then((result, error) => {

		if(error){
			
			return false

		} else {

			if(result == null){
				return false

			} else {
				
				result.password = "************"
				return result

			}
		}
	})
};

// Assign User Admin
module.exports.assignAdmin = (reqBody) => {

	let setAdmin = {
		isAdmin: true
	}

	// let setAdminLevel = {
	// 	adminLevel: reqBody.adminLevel
	// }

	return User.findOne({email: reqBody.email}).then(result => {

		if(result.isAdmin){

			return false
		} else {

			return User.findByIdAndUpdate(result._id, setAdmin).then((setUserAdmin, error) => {

				if(error){

					return false
				} else {

					return true
				}
			})
		}
	})
};