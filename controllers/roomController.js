const Room = require("../models/Room");
// const User = require("../models/User");


// Add item to rooms collection
module.exports.addRoom = (reqBody) => {

	let newRoom = new Room({
		roomNo: reqBody.roomNo,
		roomType: reqBody.roomType,
		bookingPrice: reqBody.bookingPrice
	});

	return Room.findOne({roomNo: reqBody.roomNo}).then(result => {

		if(result){

			return "Room already exists!"
		} else {

			return newRoom.save().then((room, error) => {
				if(error){
					
					return "Adding new room failed."
				} else {

					return "New room successfully added!"
				}
			})
		}
	})
};


// Get ALL rooms (admin only)
module.exports.getAllRooms = () => {

	return Room.find({}).then((result, err) => {

		if(err){

			return false
		} else {
			return result
		}
	})
}; 


// Get available rooms only
module.exports.getAvailableRooms = () => {

	return Room.find({isActive: true}).then((result, error) => {
		
		if(error){

			return false
		} else {

			return result
		}
	})
};



// Get single rooms only
module.exports.getSingleRooms = () => {

	return Room.find({roomType: 'Single'}).then((result, error) => {
		
		if(error){

			return false
		} else {

			return result
		}
	})
};


// Update room
module.exports.updateRoom = (reqParams, reqBody) => {

	let updatedRoom = {
		roomNo: reqBody.roomNo,
		roomType: reqBody.roomType,
		bookingPrice: reqBody.bookingPrice
	}


	return Room.findByIdAndUpdate(reqParams.roomId, updatedRoom).then((updatedRoom, error) => {

		if(error){

			return false
		} else {

			return "The room has been updated."
		}
	})
};



// Archive room
module.exports.archiveRoom = (reqParams, reqBody) => {

	let archivedRoom = {
		isActive: false
	}

	let unarchivedRoom = {
		isActive: true
	}

	return Room.findById(reqParams.roomId).then(result => {

		if(result.isActive){

			return Room.findByIdAndUpdate(reqParams.roomId, archivedRoom).then((archivedRoom, error) => {

				if(error){

					return false
				} else {

					return "Room is successfully archived!"
				}
			})
		} else {

			return Room.findByIdAndUpdate(reqParams.roomId, unarchivedRoom).then((unarchivedRoom, error) => {

				if(error){

					return false
				} else {

					return "Room is successfully unarchived!"
				}
			})
		}
	}) 
};
