const User = require("../models/User");
const Room = require("../models/Room");
const Transaction = require("../models/Transaction");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Add Customer Transaction
module.exports.addCheckIn = (data, reqBody) => {

	return Room.findOne({roomNo: data.roomNo}).then(result => {

		let newTransaction = new Transaction({
			userId: data.userId,
			rooms: [{
				roomNo: data.roomNo,
				days: data.days,
				subtotal: data.days * result.bookingPrice
			}]
		})

		return Transaction.findOne({userId: data.userId}).then(transactions => {

			if(transactions){

				return Transaction.findById(transactions._id).then(addTransaction => {

					addTransaction.rooms.push({
						roomNo: data.roomNo,
						days: data.days,
						subtotal: data.days * result.bookingPrice
					})

					return addTransaction.save().then((addTransaction, error) => {

						if(error){

							return false
						} else {

							return true
						}
					})
				});

				return true
			} else {

				return newTransaction.save().then((newTrans, error) => {

					if(error){

						return false
					} else {

						return true
					}
				})
			}
		})
	})	
};



// Check Out Booking
module.exports.checkOut = (data) => {
 	
 	return Transaction.findOne({userId: data.userId}).then(user => {

 		if(user) {

 			return Transaction.findById(user._id).then(result => {

 				if(result) {

 					let totalSum = 0

 					for (let i = 0; i < result.rooms.length; i++){

 						totalSum += result.rooms[i].subtotal
 					}

 					let getTotal = {
 						totalAmount: totalSum
 					}

 					return Transaction.findByIdAndUpdate(result._id, getTotal).then((getTotal, error) => {

 						if(error){

 							return false
 						} else {

 							return "Checked out successfully."
 						}
 					})
 				}

 			})
 		} else {

 			return "Unauthorized access!"
 		}
 	})	
};


// Retrieve User Transaction
module.exports.getTransaction = (data) => {
	
	return Transaction.find({userId: data.userId}).then((result, error) => {

		if(error){
			
			return false

		} else {

			return result
		}
	})
};


// Retrieve all transactions (admin access)
module.exports.getAllTransactions = () => {

	return Transaction.find({}).then((result, error) => {

		if(error){

			return false
		} else {

			return result
		}
	})
};