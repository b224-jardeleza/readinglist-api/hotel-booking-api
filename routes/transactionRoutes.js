const express = require("express");
const router = express.Router();
const transactionController = require("../controllers/transactionController");
const auth = require("../auth");


// Route for customer check-in
router.post("/check-in", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	let data = {
		userId: userData.id,
		roomNo: req.body.roomNo,
		days: req.body.days
	}

	if(userData.isAdmin){

		res.send("You are not a customer account.")
	} else {

		transactionController.addCheckIn(data).then(control => res.send(control))	
	}
	
});


// Route for check out
router.put("/check-out/:transactionId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	// if(userData.userId != req.params.userId){

		// res.send("This is not your cart!")
	// } else {

	let data = {
		transactionId: req.params._id,
		userId: userData.id
	}

		transactionController.checkOut(data).then(control => res.send(control))
	// }

});


// Route for User Transaction
router.get("/:transactionId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	let data = {
		transactionId: req.params._id,
		userId: userData.id
	}

	transactionController.getTransaction(data).then(resultFromController => res.send(resultFromController))
});


// Route for getting all transactions (admin access)
router.get("/t/all", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){

		transactionController.getAllTransactions(userData).then(control => res.send(control))
	} else {

		res.send("Unauthorized access!")
	}
});










module.exports = router;