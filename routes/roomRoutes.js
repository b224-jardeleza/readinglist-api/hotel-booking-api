const express = require("express");
const router = express.Router();
const roomController = require("../controllers/roomController");
const auth = require("../auth");

// Route for adding rooms
router.post("/add", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){

			roomController.addRoom(req.body).then(control => res.send(control))
	} else {

		res.send("Unauthorized access!")

	}
});


// Route for getting ALL rooms
router.get("/all", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){

		roomController.getAllRooms(userData).then(control => res.send(control))
	} else {

		res.send("Unauthorized access!")
	}
});


// Route for getting available rooms
router.get("/", (req, res) => {

	roomController.getAvailableRooms().then(control => res.send(control))
});


// Route for getting single rooms
router.get("/single-rooms", (req, res) => {

	roomController.getSingleRooms().then(control => res.send(control))
});


// Route for updating room
router.put("/:roomId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){

			roomController.updateRoom(req.params, req.body).then(control => res.send(control))
	} else {

		res.send("Unauthorized access!")
	}
	
});



// Route for archiving rooms
router.put("/archive/:roomId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){

			roomController.archiveRoom(req.params, req.body).then(control => res.send(control))
	} else {

		res.send("Unauthorized access!")
	}
});


module.exports = router;