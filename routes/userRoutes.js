const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth")

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user profile
router.get("/profile", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});


// Route for setting user as admin (Admin Access)
router.put("/assign-admin", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){

			userController.assignAdmin(req.body).then(resultFromController => res.send(resultFromController))
	} else {		
		res.send("Unauthorized access!")
	}
});








module.exports = router;